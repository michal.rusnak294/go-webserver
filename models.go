package main

type ErrorResponse struct {
	Error string `json:"error"`
}

type Versions struct {
	Version   string `json:"version"`
	Commit    string `json:"commit"`
	BuildTime string `json:"buildTime"`
}

type Author struct {
	Authors []Authorkey `json:"authors"`
}

type Authorkey struct {
	Key string `json:"key"`
}

type Authorname struct {
	Name string `json:"personal_name"`
}

type AuthorResponse struct {
	AuthorName string `json:"author"`
	Key        string `json:"authorKey"`
}

type Entries struct {
	Entries []Books `json:"entries"`
}
type Books struct {
	Title    string `json:"title"`
	Key      string `json:"key"`
	Revision int    `json:"revision"`
	Created  struct {
		Value string `json:"value"`
	} `json:"created"`
}
type RespBooks struct {
	Name        string `json:"name"`
	Key         string `json:"key"`
	Revision    int    `json:"revision"`
	PublishDate string `json:"publishDate"`
}
